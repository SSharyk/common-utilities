﻿using System;
using System.IO;
using Utilities.Extensions;

namespace Utilities.Log
{
	/// <summary>
	/// Contains logic for logging 
	/// </summary>
	public sealed class LogHelper: IDisposable
	{
		#region Fields and properties

		private static Stream _stream;
		public static Stream Stream 
		{
			get { return _stream; }
			set 
			{
				_stream = value;
				if (_stream.CanSeek)
					_stream.Seek(0, SeekOrigin.End);
				_writer = new StreamWriter(_stream);
			}
		}
		
		private static StreamWriter _writer;
		
		public static bool IsWithDate { get; set; }
		
		#endregion
		
		#region Logging methods
		
		/// <summary>
		/// Logs data
		/// </summary>
		/// <param name="message">Message need be logged</param>
		/// <param name="level">Level of logging of the message</param>
		/// <param name="messageType">Type of logging of the message</param>
		public static void Log(string message, int level = 0, MessageType messageType = MessageType.Info)
        {
            string tabs = " ".Repeat(level);
            string consoleMsg = string.Format("{0}{1}> {2}",
                (messageType == MessageType.None) ? ' ' : messageType.ToString()[0], tabs,
                message);
            if (IsWithDate)
            	_writer.WriteLine("{0}: {1}",  DateTime.Now, consoleMsg);
            else
            	_writer.WriteLine(consoleMsg);
        }

		/// <summary>
		/// Logs data as error
		/// </summary>
		/// <param name="source">Type of source where the error was occured</param>
		/// <param name="message">Message need be logged</param>
		/// <param name="level">Level of logging of the message</param>
        public static void Error(Type source, string message, int level = 0)
        {
            message = $"From {source.FullName}: " + message;
            bool isWithDate = IsWithDate;
            IsWithDate = true;
            Log(message, level, MessageType.Error);
            IsWithDate = isWithDate;
        }

        /// <summary>
        /// Logs empty lines
        /// </summary>
        /// <param name="count">Number of empty lines</param>
        public static void Empty(int count = 1)
        {
            while (count > 0)
            {
                _writer.WriteLine();
                count--;
            }
        }		
		
        #endregion

        #region IDisposable implementation

        public void Dispose()
        {
            _writer.Close();
        }
        
        public static void Close()
        {
        	_writer.Close();
        	_stream.Close();
        }
        
        #endregion
	}
}
