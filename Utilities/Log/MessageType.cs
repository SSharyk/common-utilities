﻿namespace Utilities.Log
{
    public enum MessageType
    {
        Error = 1,
        Info = 2,
        Warning = 3,

        None = 0
    }
}