﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace Utilities.TreeHelper
{
	/// <summary>
	/// Represents binary tree, i.e. recursive data structure with two 
	/// required fields: left child and right child
	/// </summary>
	public class BinaryTree<T>: ITree<T>
	{
		#region Fields and properties

		public ITreeNode<T> Root { get; set; }
		
		public TreeIteratingWay IteratingWay { get; set; }

		#endregion
		
		#region Construction

		/// <summary>
		/// Creates new instance of <see cref="BinaryTree>"/>
		/// </summary>
		/// <param name="root">Root of the tree</param>
		public BinaryTree(BinaryTreeNode<T> root)
			: this(root, TreeIteratingWay.Infix)
		{			
		}
		
		/// <summary>
		/// Creates new instance of <see cref="BinaryTree>"/>
		/// </summary>
		/// <param name="root">Root of the tree</param>
		/// <param name="routing">Way of iterating through elements of the tree</param>
		public BinaryTree(BinaryTreeNode<T> root, TreeIteratingWay routing)
		{
			this.Root = root;
			this.IteratingWay = routing;
		}

		/// <summary>
		/// Creates new instance of <see cref="BinaryTree"/> with 
		/// specific elements iterated in infix way
		/// </summary>
		/// <param name="entities">Collection of entities of nodes</param>
		public BinaryTree(IEnumerable<T> entities)
		{
			var nodes = entities
				.Select(x => new BinaryTreeNode<T>(x, null, null))
				.ToList();
			int height = (int) Math.Ceiling(Math.Log(nodes.Count, 2)) - 1;
			int startIndex = (int) Math.Pow(2, height);
			this.Root = ConstructTree(nodes, startIndex, height - 1);
		}
		
		private static BinaryTreeNode<T> ConstructTree(List<BinaryTreeNode<T>> nodes, int indexOfCurrentRoot, int level)
		{
			if (level == -1) 
				return (indexOfCurrentRoot < nodes.Count) ? nodes[indexOfCurrentRoot - 1] : null;
			if (indexOfCurrentRoot >= nodes.Count) return null;
			
			int offset = (int) Math.Pow(2, level);
			var left = ConstructTree(nodes, indexOfCurrentRoot - offset, level - 1);
			var right = ConstructTree(nodes, indexOfCurrentRoot + offset, level - 1);
			return new BinaryTreeNode<T>(nodes[indexOfCurrentRoot - 1].Entity, left, right);
		}
		
		#endregion	

		#region Iterating
		
		/// <summary>
		/// Executes action for each element of the tree
		/// </summary>
		/// <param name="action">Executed action</param>
		/// <param name="routing">Way of iterating through elements of the tree</param>
		public void Run(Action<T> action, TreeIteratingWay routing = TreeIteratingWay.Default)
		{
			if (routing == TreeIteratingWay.Default)
				routing = this.IteratingWay;
			Run(Root as BinaryTreeNode<T>, action, routing);
		}
		
		private void Run(BinaryTreeNode<T> root, Action<T> action, TreeIteratingWay routing = TreeIteratingWay.Default)
		{
			if (root.IsList)
			{
				action(root.Entity);
				return;
			}
			
			switch (routing)
			{
				case TreeIteratingWay.Prefix:
					action(root.Entity);
					Run(root.Left, action, routing);
					Run(root.Right, action, routing);
					break;
					
				case TreeIteratingWay.Infix:
					Run(root.Left, action, routing);
					action(root.Entity);
					Run(root.Right, action, routing);
					break;
					
				case TreeIteratingWay.Postfix:
					Run(root.Left, action, routing);
					Run(root.Right, action, routing);
					action(root.Entity);					
					break;
			}				
		}
		
		#endregion
	
		#region Convertation
		
		/// <summary>
		/// Converts tree to plain list
		/// </summary>
		/// <param name="routing">Way of iterating through elements of the tree</param>
		/// <returns>Plain list equivalent to the tree</returns>
		public List<T> ToList(TreeIteratingWay routing = TreeIteratingWay.Default)
		{
			if (routing == TreeIteratingWay.Default)
				routing = this.IteratingWay;
						
			List<T> res = new List<T>();
			this.Run(res.Add, routing);
			return res;
		}
		
		#endregion
	}
}
