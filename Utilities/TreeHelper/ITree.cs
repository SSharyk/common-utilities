﻿using System;
using System.Collections.Generic;

namespace Utilities.TreeHelper
{
	/// <summary>
	/// Contains data and logic for tree
	/// </summary>
	public interface ITree<T> 
	{
		/// <summary>
		/// Root of the tree
		/// </summary>
		ITreeNode<T> Root { get; set; }
		
		/// <summary>
		/// Way of iterating through elements of the tree
		/// </summary>
		TreeIteratingWay IteratingWay { get; set; }
		
		/// <summary>
		/// Executes action for each element of the tree
		/// </summary>
		/// <param name="action">Executed action</param>
		/// <param name="routing">Way of iterating through elements of the tree</param>
		void Run(Action<T> action, TreeIteratingWay routing = TreeIteratingWay.Default);
		
		/// <summary>
		/// Converts tree to plain list
		/// </summary>
		/// <param name="routing">Way of iterating through elements of the tree</param>
		/// <returns>Plain list equivalent to the tree</returns>
		List<T> ToList(TreeIteratingWay routing = TreeIteratingWay.Default);			
	}
}
