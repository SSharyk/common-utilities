﻿namespace Utilities.TreeHelper
{
	/// <summary>
	/// Contains constants for ways of tree elements iterating
	/// </summary>
	public enum TreeIteratingWay
	{
		Default,
		Prefix,		
		Infix,		
		Postfix
	}
}