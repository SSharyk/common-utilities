﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace Utilities.TreeHelper
{
	/// <summary>
	/// Represents node of binary tre
	/// </summary>
	public class BinaryTreeNode<T>: ITreeNode<T>
	{
		#region Fields and properties. ITreeNode implementation

		public T Entity { get; set; }
		
		public List<ITreeNode<T>> Children { get; set; }
		
		public ITreeNode<T> this[int index]
		{
			get { return Children[index]; }
			set { Children[index] = value; }
		}
		
		public BinaryTreeNode<T> Left
		{
			get { return Children[0] as BinaryTreeNode<T>; }
			set { Children[0] = value; }
		}
		
		public BinaryTreeNode<T> Right
		{
			get { return Children[1] as BinaryTreeNode<T>; }
			set { Children[1] = value; }
		}

		public bool IsList =>
			this.Children[0] == null && this.Children[1] == null;
					
		#endregion

		#region Construction
		
		/// <summary>
		/// Creates new instance of node of binary tree
		/// <param name="entity">Value of the node</param>
		/// <param name="left">Value of node in left branch</param>
		/// <param name="right">Value of node in tight branch</param>
		/// </summary>
		public BinaryTreeNode(T entity, T left = default(T), T right = default(T))
			: this(entity, new BinaryTreeNode<T>(left, null, null), new BinaryTreeNode<T>(right, null, null))
		{
		}

		/// <summary>
		/// Creates new instance of node of binary tree
		/// </summary>
		/// <param name="entity"></param>
		/// <param name="left"></param>
		/// <param name="right"></param>
		public BinaryTreeNode(T entity, BinaryTreeNode<T> left = null, BinaryTreeNode<T> right = null)
		{
			this.Entity = entity;
			this.Children = new List<ITreeNode<T>>(2) { null, null };
			this.Left = left;
			this.Right = right;
		}
		
		#endregion
	}
}