﻿using System;
using System.Collections.Generic;

namespace Utilities.TreeHelper
{
	/// <summary>
	/// Contains data and logic for each element of the tree 
	/// </summary>
	public interface ITreeNode<T>
	{
		/// <summary>
		/// Gets or sets content of the node
		/// </summary>
		T Entity { get; set; }
		
		/// <summary>
		/// Gets or sets childrens of the specific item
		/// </summary>
		List<ITreeNode<T>> Children {get; set; }
		
		/// <summary>
		/// Returns children with specific index
		/// </summary>
		ITreeNode<T> this[int index] { get; set; }
		
		/// <summary>
		/// Returns <c>True</c> if element is a list of the tree
		/// </summary>
		bool IsList { get; }
	}
}
