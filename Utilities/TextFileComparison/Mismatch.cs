﻿using System;

namespace Utilities.TextFileComparison
{
	/// <summary>
	/// <see cref="Mismatch"/> contains data for files (non-)equality 
	/// </summary>
	public struct Mismatch
    {
        public int Line;
        public int Symbol;
        public MismatchType Error;

        public bool AreFilesEqual =>
        	Error == MismatchType.ФайлыРавны;

        public string Description => ToString();

        public override string ToString()
        {
            return string.Format("{0} : [{1}; {2}]", Error, Line, Symbol);
        }
    }
}
