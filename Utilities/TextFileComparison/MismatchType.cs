﻿namespace Utilities.TextFileComparison
{
    public enum MismatchType
    {
        СимволПервогоФайлаБольше,
        СимволВторогоФайлаБольше,

        КонецФайла,
        КонецФайла1,
        КонецФайла2,

        ФайлНеСуществует,

        ФайлыРавны
    }
}