﻿using System;
using System.Linq;
using System.Text;
using System.IO;

namespace Utilities.TextFileComparison
{
	/// <summary>
	/// Implements comparing of two text files
	/// </summary>
    public static class TextFileComparator
    {
    	/// <summary>
    	/// Compares files
    	/// </summary>
    	/// <param name="path1">Full path to the first file</param>
    	/// <param name="path2">Full path to the second file</param>
    	/// <returns><see cref="Mismatch"/> struct shows comparing result</returns>
        public static Mismatch CompareTextFiles(string path1, string path2)
        {
            FileStream stream1, stream2;
            StreamReader reader1, reader2;
            int line = 0;

            try
            {
                using (stream1 = new FileStream(path1, FileMode.Open, FileAccess.Read))
                {
                    using (stream2 = new FileStream(path2, FileMode.Open, FileAccess.Read))
                    {

                        reader1 = new StreamReader(stream1, Encoding.Default);
                        reader2 = new StreamReader(stream2, Encoding.Default);

                        string allText1 = reader1.ReadToEnd();
                        string allText2 = reader2.ReadToEnd();
                        if (allText1 == allText2)
                        {
                            return new Mismatch
                               {
                                   Error = MismatchType.ФайлыРавны,
                                   Line = 0,
                                   Symbol = 0
                               };
                        }

                        var lines1 = allText1.Split('\n');
                        var lines2 = allText2.Split('\n');
                        int len = Math.Min(lines1.Length, lines2.Length);
                        for (; line < len; line++)
                        {
                            int comp = lines1[line].CompareTo(lines2[line]);
                            if (comp != 0)
                            {
                                Mismatch mismatch = GetSymbolMismatch(lines1[line], lines2[line]);
                                mismatch.Line = line + 1;
                                if (mismatch.Symbol == 0)
                                {
                                    mismatch.Symbol = (lines1[line].Length > lines2[line].Length) 
                                        ? lines2[line].Length 
                                        : lines1[line].Length;
                                    continue;
                                }
                                return mismatch;
                            }
                        }
                        return new Mismatch
                        {
                            Error = (lines1.Length > lines2.Length) 
                                ? MismatchType.КонецФайла2 
                                : MismatchType.КонецФайла1,
                            Line = len,
                            Symbol = 0
                        };
                    }
                }
            }
            catch (FileNotFoundException)
            {
                return new Mismatch
                {
                    Error = MismatchType.ФайлНеСуществует,
                    Line = 0,
                    Symbol = 0
                };
            }
            catch (Exception)
            {
                return new Mismatch
                {
                    Error = MismatchType.КонецФайла,
                    Line = line,
                    Symbol = 0
                };
            }
        }

        private static Mismatch GetSymbolMismatch(string line1, string line2)
        {
            int len = Math.Min(line1.Length, line2.Length);
            for (int i = 0; i < len; i++)
            {
                var comp = line1[i].CompareTo(line2[i]);
                if (comp != 0)
                {
                    return new Mismatch
                    {
                        Error = (comp > 0)
                            ? MismatchType.СимволПервогоФайлаБольше
                            : MismatchType.СимволВторогоФайлаБольше,
                        Symbol = i + 1
                    };
                }
            }
            return new Mismatch();
        }
    }
}