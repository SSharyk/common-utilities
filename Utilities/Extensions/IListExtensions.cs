﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace Utilities.Extensions
{
	/// <summary>
	/// Set of extensions for <see cref="List"/> interface 
	/// and its implementstions
	/// </summary>
	public static class IListExtensions
	{
		#region Toggle
		
		/// <summary>
		/// Toggles item presenting in the list
		/// </summary>
		/// <param name="list">Input list</param>
		/// <param name="item">Item need be added/removed</param>
		public static void Toggle<TSource>(this IList<TSource> list, 
		                                   TSource item)
		{
			if (list.Contains(item))
				list.Remove(item);
			else
				list.Add(item);
		}
		
		#endregion
		
		#region Removing
		
		/// <summary>
		/// Removes from list all occurences of the specific item
		/// </summary>
		/// <param name="list">Input collection</param>
		/// <param name="item">Item need be deleted</param>
		public static void RemoveAll<TSource>(this IList<TSource> list,
		                                      TSource item)
		{
			while(list.Contains(item))
				list.Remove(item);
		}
		
		/// <summary>
		/// Removes from the list all items form another enumeration 
		/// </summary>
		/// <param name="list">Input collection</param>
		/// <param name="deleteList">Collection with items need be removed</param>
		/// <param name="isFirstOnly">If <c>True</c>, all occurences of the item would be deleted; 
		/// otherwise, the first one only</param>
		public static void Remove<TSource>(this IList<TSource> list,
		                                   IEnumerable<TSource> deleteList,
		                                   bool isFirstOnly = false)
		{
			foreach (var delItem in deleteList)
			{
				if (list.Contains(delItem))
				{
					if (isFirstOnly)
						list.Remove(delItem);
					else
						list.RemoveAll(delItem);
				}
			}
		}
		
		#endregion
	}
}
