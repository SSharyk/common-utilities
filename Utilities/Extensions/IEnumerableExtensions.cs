﻿using System;
using System.Collections.Generic;

namespace Utilities.Extensions
{
	/// <summary>
	/// Set of extensions for <see cref="Enumerable"/> interface 
	/// and its implementstions
	/// </summary>
	public static class IEnumerableExtensions
	{
		#region ForEach

		/// <summary>
		/// Executes action for each item in the collection
		/// </summary>
		/// <param name="sequence"></param>
		/// <param name="action"></param>
		public static void ForEach<T>(this IEnumerable<T> sequence,
		                              Action<T> action)
		{
			if (action == null)
				throw new NullReferenceException("Action is NULL");
			foreach (T item in sequence)
                action(item);
		}
		
		/// <summary>
		/// Executes action for each item in the collection
		/// </summary>
		/// <param name="sequence"></param>
		/// <param name="action"></param>
		public static void ForEach<T>(this IEnumerable<T> sequence,
		                              Action<int, T> action)
		{
			if (action == null)
				throw new NullReferenceException("Action is NULL");
			
			int i = 0;
            foreach (T item in sequence)
            {
                action(i, item);
                i++;
            }
		}
		
		#endregion
	}
}
