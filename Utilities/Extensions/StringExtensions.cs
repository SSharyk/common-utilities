﻿using System;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;

namespace Utilities.Extensions
{
	/// <summary>
	/// Set of extensions for <see cref="String"/>s
	/// </summary>
	public static class StringExtensions
	{
		/// <summary>
		/// Repeats input string for specific times
		/// </summary>
		/// <param name="s">Input string</param>
		/// <param name="count">Number of repeatings</param>
		/// <returns>Input string repeated count times</returns>
		public static string Repeat(this string s, int count)
		{
			if (s == null) throw new NullReferenceException(
				$"Input string {nameof(s)} is NULL");
			if (count < 0) throw new ArgumentOutOfRangeException(
				nameof(count), "Count should be not-negative number");
			
			if (count == 0)	return "";
			if (count == 1)	return s;
			StringBuilder builder = new StringBuilder(s);
			for (int k = 1; k < count; k++)
			{
				builder.Append(s);
			}
			return builder.ToString();
		}
		
		/// <summary>
		/// Normalizes string by deleting double spaces
		/// </summary>
		/// <param name="s">Input string</param>
		/// <returns>New string where all occurrences of multi spaces replaces with the single one</returns>
		public static string NormalizeSpaces(this string s)
		{
			return Regex.Replace(s, "\\s+", " ");
		}
		
		/// <summary>
        /// Removes specfic substring from string
        /// </summary>
        /// <param name="s">Input string</param>
        /// <param name="c">Specific substring</param>
        /// <returns>New string in which all occurrences of a specific substring are replaced with empty substring</returns>
        public static string Remove(this string s, string c)
        {
            return s.Replace(c, @"");
        }
	
        #region Capitalize/decapitalize
        
        private static readonly char[] _delimeters = new char[] {' ', '(', ')', '[', ']'};

        /// <summary>
        /// Removes spaces and converts first letters of each word
        /// </summary>
        /// <param name="s">Input string</param>
        /// <param name="isJoin">If <c>True</c>, all words will be joined t the single word</param>
        /// <returns>New string which is the only word (with uppercase letters inside the word)</returns>
        public static string Capitalize(this string s, bool isJoin = true)
        {
            if (String.IsNullOrEmpty(s)) return "";

            string res = "";
            res += Char.ToUpper(s[0]);

            for (int i = 1; i < s.Length; i++)
            {
                res += (_delimeters.Contains(s[i - 1]) 
            	        ? Char.ToUpper(s[i]) 
            	        : s[i]);
            }
            if (isJoin) res = res.Remove(" ");

            return res;
        }

        /// <summary>
        /// Adds spaces and converts first letters of each word
        /// </summary>
        /// <param name="s">Input string</param>
        /// <returns>Correct case string</returns>
        public static string Decapitalize(this string s)
        {
            string res = "";
            res += s[0];

            for (int i = 1; i < s.Length; i++)
            {
            	res += ((Char.ToUpper(s[i]) == s[i] && s[i] != ' ')
            	        ? " " + Char.ToLower(s[i])
            	        : s[i].ToString());
            }

            return res;
        }
        
        #endregion
	}
}
