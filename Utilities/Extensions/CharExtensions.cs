﻿using System;

namespace Utilities.Extensions
{
	/// <summary>
	/// Set of extensions for <see cref="Char"/>s
	/// </summary>
	public static class CharExtensions
	{
		/// <summary>
		/// Checks if the specific character is in range
		/// </summary>
		/// <param name="c">Input character</param>
		/// <param name="left">Left bound of the range</param>
		/// <param name="right">Right bound of the range</param>
		/// <param name="isIncluding"><c>True</c> if bounds are included into the range</param>
		/// <returns><c>True</c> if the specific character is in range</returns>
		public static bool Between(this char c, char left, char right, bool isIncluding = true)
		{
			if (left > right) return c.Between(right, left);
			return ((isIncluding)
				? (left <= c && c <= right)
				: (left <  c && c <  right));
		}
	}
}
