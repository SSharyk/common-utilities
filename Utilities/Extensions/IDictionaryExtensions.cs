﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Utilities.Extensions
{
	/// <summary>
	/// Set of extensions for <see cref="Dictionary"/> interface 
	/// and its implementstions
	/// </summary>
	public static class IDictionaryExtensions
	{
		private const string DEFAULT_KEY_VALUE_SEPARATOR = " => ";
		private const string DEFAULT_LINES_SEPARATOR = "\r\n";
		
		/// <summary>
		/// Get pretty string representation of the dictionary
		/// </summary>
		/// <param name="dictionary">Input dictionary</param>
		/// <param name="keyValuePairSeparator">Symbol between key and value</param>
		/// <param name="linesSeparator">Symbol between pairs</param>
		/// <returns>String of pretty representation of the dictionary</returns>
		public static string ToString<TKey, TValue>(this IDictionary<TKey, TValue> dictionary,
		                                            string keyValuePairSeparator = DEFAULT_KEY_VALUE_SEPARATOR,
		                                            string linesSeparator = DEFAULT_LINES_SEPARATOR)
		{
			if (keyValuePairSeparator == null) keyValuePairSeparator = DEFAULT_KEY_VALUE_SEPARATOR;
			if (linesSeparator == null) linesSeparator = DEFAULT_LINES_SEPARATOR;
			
			StringBuilder s = new StringBuilder();
			for (int i = 0; i < dictionary.Keys.Count; i++)
			{
				var key = dictionary.Keys.ElementAt(i);
				s.Append($"{key}{keyValuePairSeparator}{dictionary[key]}");
				if (i != dictionary.Keys.Count - 1) 
					s.Append(linesSeparator);
			}
			return s.ToString();
		}
	}
}
