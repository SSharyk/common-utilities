﻿using System;

namespace Utilities.Extensions
{
	/// <summary>
	/// Set of extensions for arrays
	/// </summary>
	public static class ArrayExtensions
	{
		#region Copy

		/// <summary>
		/// Creates new 1D array based on existing
		/// </summary>
		/// <param name="array">Original</param>
		/// <returns>New array with the same elements</returns>
        public static T[] Copy<T>(this T[] array)
        {
            var res = new T[array.Length];
            for (int i = 0; i < array.Length; i++)
                res[i] = array[i];
            return res;
        }
        
		/// <summary>
		/// Creates new 2D array based on existing
		/// </summary>
		/// <param name="array">Original</param>
		/// <returns>New array with the same elements</returns>
        public static T[,] Copy<T>(this T[,] array)
        {
        	int h = array.GetLength(0);
        	int w = array.GetLength(1);
        	var res = new T[h, w];
            for (int i = 0; i < h; i++)
            	for (int j = 0; j < w; j++)
                	res[i, j] = array[i, j];
            return res;
        }
        
        #endregion
	
        #region Clear

		/// <summary>
		/// Resets array with specific value
		/// </summary>
		/// <param name="array">Input array</param>
		/// <param name="value">Value need be setted to each element of array</param>
        public static void Clear<T>(this T[] array, T value = default(T))
        {
            for (int i = 0; i < array.Length; i++)
                array[i] = value;
        }

        /// <summary>
		/// Resets array with specific value
		/// </summary>
		/// <param name="array">Input array</param>
		/// <param name="value">Value need be setted to each element of array</param>
        public static void Clear<T>(this T[,] array, T value = default(T))
        {
            for (int x = 0; x < array.GetLength(0); x++)
            {
                for (int y = 0; y < array.GetLength(1); y++)
                {
                    array[x, y] = value;
                }
            }
        }
        
        #endregion    
	
        #region Getting subarrays
		
        /// <summary>
        /// Returns row of two-dimensional array with the specific index
        /// </summary>
        /// <param name="array">Input array</param>
        /// <param name="rowIndex">Index of row should be returned (0-based)</param>
        /// <returns>One-dimensional array represents the row</returns>
		public static T[] GetRow<T>(this T[,] array, int rowIndex)
		{
			if (rowIndex < 0)
				throw new ArgumentOutOfRangeException(nameof(rowIndex), "Index should be not-negative");
			if (rowIndex >= array.GetLength(0))
				throw new ArgumentOutOfRangeException(nameof(rowIndex), "Index should be less than height of the array");
			
			int l = array.GetLength(1);
			T[] res = new T[l];
			for (int j = 0; j < l; j++)
				res[j] = array[rowIndex, j];
			return res;
		}
		
		/// <summary>
        /// Returns column of two-dimensional array with the specific index
        /// </summary>
        /// <param name="array">Input array</param>
        /// <param name="columnIndex">Index of column should be returned (0-based)</param>
        /// <returns>One-dimensional array represents the column</returns>
		public static T[] GetColumn<T>(this T[,] array, int columnIndex)
		{
			if (columnIndex < 0)
				throw new ArgumentOutOfRangeException(nameof(columnIndex), "Index should be not-negative");
			if (columnIndex >= array.GetLength(1))
				throw new ArgumentOutOfRangeException(nameof(columnIndex), "Index should be less than width of the array");
				
			int l = array.GetLength(0);
			T[] res = new T[l];
			for (int i = 0; i < l; i++)
				res[i] = array[i, columnIndex];
			return res;
		}
				
		/// <summary>
		/// Returns subarray of one-dmensional array
		/// </summary>
		/// <param name="array">Input array</param>
		/// <param name="startIndex">Index of left bound</param>
		/// <param name="length">Length of the extracted range</param>
		/// <returns>Subarray range</returns>
		public static T[] GetRange<T>(this T[] array, int startIndex, int length)
		{
			if (startIndex < 0 || startIndex >= array.Length)
				throw new IndexOutOfRangeException();
			if (startIndex + length > array.Length)
				throw new ArgumentOutOfRangeException();
			
			T[] res = new T[length];
			for (int i = 0; i < length; i++)
				res[i] = array[i + startIndex];
			return res;
		}
		
		#endregion
		
		#region Convertation
		
		/// <summary>
        /// Converts two-dimensional array to the one-dimentional array
        /// </summary>
        /// <param name="array">2D array</param>
        /// <returns>1D array</returns>
        public static T[] ToOneDimensionArray<T>(this T[,] array)
        {
            T[] res = new T[array.Length];
            int k = -1;
            for (int i = 0; i < array.GetLength(0); i++)
            {
                for (int j = 0; j < array.GetLength(1); j++)
                {
                    res[++k] = array[i, j];
                }
            }
            return res;
        }
		
		#endregion
	
		#region Adding
		
		/// <summary>
		/// Adds row to the array and returns new array
		/// </summary>
		/// <param name="array">Input array</param>
		/// <returns>New array with added row</returns>
		public static T[,] AddRow<T>(this T[,] array)
		{
			var row = new T[array.GetLength(1)];
			for (int i = 0; i < row.Length; i++)
				row[i] = default(T);
			return array.AddRow(row);
		}
		
		/// <summary>
		/// Adds row to the array and returns new array
		/// </summary>
		/// <param name="array">Input array</param>
		/// <param name="row">Added row</param>
		/// <returns>New array with added row</returns>
		public static T[,] AddRow<T>(this T[,] array, T[] row)
		{
			if (row == null)
				throw new ArgumentNullException(nameof(row));
			if (row.Length != array.GetLength(1))
				throw new ArgumentException("Invalid length of new row");
			
			T[,] arrNew = new T[array.GetLength(0) + 1, array.GetLength(1)];
			for (int i = 0; i < array.GetLength(0); i++)
			{
				for (int j = 0; j < array.GetLength(1); j++)
				{
					arrNew[i, j] = array[i, j];
				}
			}
			for (int j = 0; j < array.GetLength(1); j++)
			{
				arrNew[arrNew.GetLength(0) - 1, j] = row[j];
			}
			return arrNew;
		}
		
				
		/// <summary>
		/// Adds column to the array and returns new array
		/// </summary>
		/// <param name="array">Input array</param>
		/// <returns>New array with added column</returns>
		public static T[,] AddColumn<T>(this T[,] array)
		{
			var column = new T[array.GetLength(0)];
			for (int i = 0; i < column.Length; i++)
				column[i] = default(T);
			return array.AddColumn(column);
		}
		
		/// <summary>
		/// Adds column to the array and returns new array
		/// </summary>
		/// <param name="array">Input array</param>
		/// <param name="column">Added column</param>
		/// <returns>New array with added column</returns>
		public static T[,] AddColumn<T>(this T[,] array, T[] column)
		{
			if (column == null)
				throw new ArgumentNullException(nameof(column));
			if (column.Length != array.GetLength(0))
				throw new ArgumentException("Invalid length of new column");
			
			int height = array.GetLength(0);
			int width = array.GetLength(1);
			T[,] newArray = new T[height, width + 1];
			for (int i = 0; i < height; i++)
			{
				for (int j = 0; j < width; j++)
				{
					newArray[i, j] = array[i, j];
				}
				newArray[i, width] = column[i];
			}
			return newArray;
		}		
		
		#endregion		
	}
}
