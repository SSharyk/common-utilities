﻿using System;
using System.Collections.Generic;
using System.Linq;
using NUnit.Framework;

using Utilities.Extensions;

namespace Utilities.Tests
{
	[TestFixture]
	public class IDictionaryExtensionsTests
	{
		#region Init

		private static readonly Dictionary<string, int> DICTIONARY = 
			new Dictionary<string, int>();
		
		static IDictionaryExtensionsTests()
		{
			var elements = new string[] { "Gold", "Silver", "Bronze" };
			DICTIONARY = elements.ToDictionary(it => it, it => it.Length);
		}

		#endregion

		#region ToString
		
		[Test]
		[TestCase("-->", "; ", "Gold-->4; Silver-->6; Bronze-->6")]
		[TestCase(null, "; ", "Gold => 4; Silver => 6; Bronze => 6")]
		[TestCase("-->", null, "Gold-->4\r\nSilver-->6\r\nBronze-->6")]
		[TestCase(null, null, "Gold => 4\r\nSilver => 6\r\nBronze => 6")]
		public void ToStringTests(string kvSep, string linesSep, string result)
		{
			var s = DICTIONARY.ToString(kvSep, linesSep);
			Assert.AreEqual(result, s);
		}
		
		#endregion
	}
}
