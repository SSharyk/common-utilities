﻿using System;
using System.IO;
using NUnit.Framework;

using Utilities.Log;

namespace Utilities.Tests
{
	[TestFixture]
	public class LogTests
	{
		private const string PATH_TO_FILE = "../../logger.log";
		private static FileStream STREAM;
		
		private void Prepare()
		{
			if (File.Exists(PATH_TO_FILE))
				File.Delete(PATH_TO_FILE);
			
			STREAM = new FileStream("../../logger.log", 
			                        FileMode.OpenOrCreate, 
			                        FileAccess.ReadWrite);
		}
		
		[Test]
		public void LogTests_File()
		{
			Prepare();
			LogHelper.Stream = STREAM;
			LogHelper.Log("Test 1");
			LogHelper.Empty(2);
			LogHelper.Error(this.GetType(), "Error log message", 1);
			LogHelper.Empty();
			LogHelper.Log("Test 2");
			LogHelper.Close();
				
			string content = "";
			using (FileStream stream = new FileStream("../../logger.log", 
			                                          FileMode.Open, FileAccess.Read))
			{
				using (StreamReader reader = new StreamReader(stream))
				{
					content = reader.ReadToEnd();
					reader.Close();
				}				
				stream.Close();
			}
			
			string line1 = "I> Test 1\r\n";
			string line2 = "\r\n\r\n";
			string line3 = DateTime.Now + ": E > From Utilities.Tests.LogTests: Error log message\r\n";
			string line4 = "\r\n";
			string line5 = "I> Test 2\r\n";
			string result = line1 + line2 + line3 + line4 + line5;
			Assert.AreEqual(result, content);
		}
	}
}
