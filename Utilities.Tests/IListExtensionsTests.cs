﻿using System;
using System.Collections.Generic;
using System.Linq;
using NUnit.Framework;

using Utilities.Extensions;

namespace Utilities.Tests
{
	[TestFixture]
	public class IListExtensionsTests
	{
		private static readonly IList<int> LIST = 
			new List<int>() { 0, 1, 2, 3, 4, 5, 6, 7, 8, 9 };
		
		#region Toggle
		
		[Test]
		[TestCase(1, new int[] { 0, 2, 3, 4, 5, 6, 7, 8, 9 })]
		[TestCase(10, new int[] { 0, 2, 3, 4, 5, 6, 7, 8, 9, 10 })]
		public void ToggleTests(int item, int[] res)
		{
			LIST.Toggle(item);
			CollectionAssert.AreEquivalent(res, LIST);
		}
		
		#endregion
		
		#region Removing
		
		[Test]
		[TestCase(0, new int[] { 0, 3, 7}, 0)]
		[TestCase(0, new int[] { }, 0)]
		[TestCase(10, new int[] { 0, 3, 7}, 10)]
		public void RemoveAllTests(int insertedItem, int[] insertionPlaces, int deletedItem)
		{
			var list = new List<int>(LIST);
			foreach (var place in insertionPlaces)
				list.Insert(place, insertedItem);			
			list.RemoveAll(deletedItem);
			
			var res = new List<int>(LIST);
			res.Remove(deletedItem);
			
			CollectionAssert.AreEquivalent(res, list);
		}
		
		[Test]
		[TestCase(new int[] { 0, 1, 2, 3, 4 }, new int[] { 5, 6, 7, 8, 9 })]
		[TestCase(new int[] { 0, 3, 5, 7, 11 }, new int[] { 1, 2, 4, 6, 8, 9 })]
		public void RemoveListItemsTests_First(int[] remove, int[] result)
		{
			var list = new List<int>(LIST);
			list.Remove(remove, true);
			
			CollectionAssert.AreEquivalent(result, list);
		}
		
		[Test]
		[TestCase(0, new int[] { 0, 1, 2, 3, 4 }, new int[] { 5, 6, 7, 8, 9 })]
		[TestCase(0, new int[] { 0, 3, 5, 7, 11 }, new int[] { 1, 2, 4, 6, 8, 9 })]
		[TestCase(10, new int[] { 0, 1, 2, 3, 4 }, new int[] { 5, 6, 7, 8, 9, 10 })]
		public void RemoveListItemsTests_All(int add, int[] remove, int[] result)
		{
			var list = new List<int>(LIST);
			list.Add(add);
			list.Remove(remove);
			
			CollectionAssert.AreEquivalent(result, list);
		}
		#endregion
	}
}