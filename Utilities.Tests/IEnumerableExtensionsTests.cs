﻿using System;
using System.Collections.Generic;
using NUnit.Framework;

using Utilities.Extensions;

namespace Utilities.Tests
{
	[TestFixture]
	public class IEnumerableExtensionsTests
	{
		private static readonly IEnumerable<int> LIST = 
			new List<int>() { 0, 1, 2, 3, 4, 5, 6, 7, 8, 9 };
		
		#region For each method
				
		[Test]
		public void ForEachTests_Items()
		{
			var list = new List<int>(LIST);
			string res = "";
			list.ForEach((index, item) => res += (item * 2).ToString());
			Assert.AreEqual("024681012141618", res);
		}
		
		[Test]
		public void ForEachTests_IndexesItems()
		{
			var list = new List<int>(LIST);
			string res = "";
			list.ForEach((index, item) => res += (item + index).ToString());
			Assert.AreEqual("024681012141618", res);
		}
		
		#endregion
	}
}
