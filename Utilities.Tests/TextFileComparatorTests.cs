﻿using System;
using System.IO;
using System.Text;

using NUnit.Framework;

using Utilities.TextFileComparison;
	
namespace Utilities.Tests
{
	[TestFixture]
	public class TextFileComparatorTests
	{
		private const string FILE_PATH_1 = "..\\..\\inp1.txt";
		private const string FILE_PATH_2 = "..\\..\\inp2.txt";
		
		[Test]
		public void TextFileComparatorTests_NoFile()
		{
			var comp = TextFileComparator.CompareTextFiles(FILE_PATH_1, FILE_PATH_2);
			File.Delete(FILE_PATH_1);
			File.Delete(FILE_PATH_2);
			
			var expected = new Mismatch {
				Line = 0,
				Symbol = 0,
				Error = MismatchType.ФайлНеСуществует
			};
			Assert.AreEqual(true, comp.Equals(expected));
		}
				
		[Test]
		public void TextFileComparatorTests_FilesEquality()
		{
			this.WriteTextToFile(FILE_PATH_1, "abcd\nefg\nhijklm");
			this.WriteTextToFile(FILE_PATH_2, "abcd\nefg\nhijklm");
			
			var comp = TextFileComparator.CompareTextFiles(FILE_PATH_1, FILE_PATH_2);
			File.Delete(FILE_PATH_1);
			File.Delete(FILE_PATH_2);
			
			var expected = new Mismatch {
				Line = 0,
				Symbol = 0,
				Error = MismatchType.ФайлыРавны
			};
			Assert.AreEqual(true, comp.Equals(expected));
		}

		[Test]
		public void TextFileComparatorTests_FilesNotEqual_Content()
		{
			this.WriteTextToFile(FILE_PATH_1, "abcd\nefg\nhijkLm");
			this.WriteTextToFile(FILE_PATH_2, "abcd\nefg\nhijklm");
			
			var comp = TextFileComparator.CompareTextFiles(FILE_PATH_1, FILE_PATH_2);
			File.Delete(FILE_PATH_1);
			File.Delete(FILE_PATH_2);
			
			var expected = new Mismatch {
				Line = 3,
				Symbol = 5,
				Error = MismatchType.СимволВторогоФайлаБольше
			};
			Assert.AreEqual(true, comp.Equals(expected));
		}
		
		[Test]
		public void TextFileComparatorTests_FilesNotEqual_Length()
		{
			this.WriteTextToFile(FILE_PATH_1, "abcd\nefg");
			this.WriteTextToFile(FILE_PATH_2, "abcd\nefg\nhijklm");
			
			var comp = TextFileComparator.CompareTextFiles(FILE_PATH_1, FILE_PATH_2);
			File.Delete(FILE_PATH_1);
			File.Delete(FILE_PATH_2);
			
			var expected = new Mismatch {
				Line = 3,
				Symbol = 0,
				Error = MismatchType.КонецФайла1
			};
			Assert.AreEqual(true, comp.Equals(expected));
		}
		
		private void WriteTextToFile(string path, string text)
		{
			using (var stream = new FileStream(path, FileMode.OpenOrCreate, FileAccess.Write))
			{
				using (var writer = new StreamWriter(stream, Encoding.Default))
				{
					writer.WriteLine(text);
					writer.Close();
				}
				stream.Close();
			}
		}
	}
}