﻿using System;
using NUnit.Framework;

using Utilities.Extensions;

namespace Utilities.Tests
{
	[TestFixture]
	public class StringExtensionsTests
	{
		#region Repeat
		
		/// <summary>
		/// Tests repeating strings with incorrect input data
		/// </summary>
		/// <param name="input">Input string need be repeated</param>
		/// <param name="count">Number of repeatings</param>
		/// <param name="exceptionType">Type of exception need be thrown</param>
		[Test]
		[TestCase("", -1, typeof(ArgumentOutOfRangeException))]
		[TestCase(null, 2, typeof(NullReferenceException))]
		public void RepeatTest_Exception(string input, int count, Type exceptionType)
		{
			Assert.Throws(exceptionType, () => input.Repeat(count));
		}

		/// <summary>
		/// Tests repeating strings with correct input data
		/// </summary>
		/// <param name="input">Input string need be repeated</param>
		/// <param name="count">Number of repeatings</param>
		/// <param name="output">Expected result</param>
		[Test]		
		[TestCase("", 2, "")]
		[TestCase("12", 1, "12")]
		[TestCase("12", 0, "")]
		[TestCase("12", 2, "1212")]
		[TestCase("12", 3, "121212")]
		public void RepeatTests_Success(string input, int count, string output)
		{
			var res = input.Repeat(count);
			Assert.AreEqual(output, res);
		}
		
		#endregion
		
		#region Spaces normalization
		
		[Test]
		[TestCase("ab   c d", "ab c d")]
		[TestCase("abcd", "abcd")]
		[TestCase("ab   c\td", "ab c d")]
		[TestCase("ab   c\r\nd", "ab c d")]
		public void NormalizeSpaceTests(string input, string output)
		{
			Assert.AreEqual(output, input.NormalizeSpaces());
		}
		
		#endregion
	
		#region Remove
		
		[Test]
		[TestCase("abc de", " ", "abcde")]
		[TestCase("abc ad abd", "ab", "c ad d")]
		public void RemoveTests(string input, string delete, string output)
		{
			var result = input.Remove(delete);
			Assert.AreEqual(output, result);
		}
		
		#endregion
		
		#region Capitalize/decapitalize
		
		[Test]
		[TestCase("this is test", false, "This Is Test")]
		[TestCase("this is test", true, "ThisIsTest")]
		[TestCase("test", false, "Test")]
		[TestCase("TEST", false, "TEST")]
		public void CapitilizeTests(string input, bool isJoin, string output)
		{
			var result = input.Capitalize(isJoin);
			Assert.AreEqual(output, result);
		}
		
		[Test]
		[TestCase("ThisIsTest", "This is test")]
		[TestCase("Test", "Test")]
		[TestCase("TEST", "T e s t")]
		public void DecapitilizeTests(string input, string output)
		{
			var result = input.Decapitalize();
			Assert.AreEqual(output, result);
		}

		#endregion
	}
}
