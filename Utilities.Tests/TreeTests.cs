﻿using System;
using System.Collections.Generic;
using System.Linq;
using NUnit.Framework;

using Utilities.TreeHelper;

namespace Utilities.Tests
{
	[TestFixture]
	public class TreeTests
	{			
		private BinaryTree<T> Prepare<T>(T root,
			T left, T l11, T l12, T right, T r11, T r12)
		{
			var leftBranch = new BinaryTreeNode<T>(left, 
				                 new BinaryTreeNode<T>(l11, null as BinaryTreeNode<T>, null),
				                 new BinaryTreeNode<T>(l12, null as BinaryTreeNode<T>, null));
			var rightBranch = new BinaryTreeNode<T>(right, 
				                 new BinaryTreeNode<T>(r11, null as BinaryTreeNode<T>, null),
				                 new BinaryTreeNode<T>(r12, null as BinaryTreeNode<T>, null));
			var rootNode = new BinaryTreeNode<T>(root, leftBranch, rightBranch);
			return new BinaryTree<T>(rootNode);
		}
		
		#region Creation

		[Test]
		[TestCase(1, 2, 3, 4, 5, 6, 7)]
		public void TreeTests_Creation<T>(T root,
			T left, T l11, T l12, T right, T r11, T r12)
		{	
			var tree = Prepare(root, left, l11, l12, right, r11, r12);
			Assert.AreEqual(TreeIteratingWay.Infix, tree.IteratingWay);
			Assert.AreEqual(root, tree.Root.Entity);
			Assert.AreEqual(left, tree.Root.Children[0].Entity);
			Assert.AreEqual(right, tree.Root.Children[1].Entity);
			Assert.AreEqual(l11, tree.Root.Children[0].Children[0].Entity);
			Assert.AreEqual(r12, tree.Root.Children[1].Children[1].Entity);
			Assert.AreEqual(null, tree.Root.Children[1].Children[0].Children[0]);
		}
		
		[Test]
		[TestCase(14, 8, 7, 11)]
		[TestCase(25, 16, 14, 22)]
		[TestCase(32, 16, 14, 22)]
		[TestCase(60, 32, 28, 44)]
		public void TreeTests_CreationFromList(int len, int rootEnt, int lrrEnt, int rlrEnt)
		{
			var list = Enumerable.Range(1, len);
			var tree = new BinaryTree<int>(list);
			Assert.AreEqual(rootEnt, tree.Root.Entity);
			Assert.AreEqual(lrrEnt, tree.Root.Children[0].Children[1].Children[1].Entity);
			Assert.AreEqual(rlrEnt, tree.Root.Children[1].Children[0].Children[1].Entity);
		}
		
		#endregion
		
		#region IsList
		
		[Test]
		[TestCase(1, 2, 3, false)]
		[TestCase(1, 0, 3, false)]
		[TestCase(1, 2, null, false)]
		[TestCase(1, null, null, true)]
		public void TreeNodeTests_IsList(int root, int left, int right, bool isList)
		{
			BinaryTreeNode<int> node = 
				new BinaryTreeNode<int>(
					root, 
					(left == 0) ? null : new BinaryTreeNode<int>(left, null as BinaryTreeNode<int>, null),
					(right == 0) ? null : new BinaryTreeNode<int>(right, null as BinaryTreeNode<int>, null));
			bool isListAct = node.IsList;
			Assert.AreEqual(isList, isListAct);
		}
		
		#endregion
		
		#region Iterating
		
		[Test]
		[TestCase(TreeIteratingWay.Prefix, new int[] { 1, 2, 3, 4, 5, 6, 7 } )]
		[TestCase(TreeIteratingWay.Infix, new int[] { 3, 2, 4, 1, 6, 5, 7 } )]
		[TestCase(TreeIteratingWay.Postfix, new int[] { 3, 4, 2, 6, 7, 5, 1 } )]
		public void TreeTests_Run(TreeIteratingWay iteratingWay, int[] list)
		{
			var tree = Prepare(1, 2, 3, 4, 5, 6, 7);
			var plain = new List<int>();
			tree.Run(x => plain.Add(x), iteratingWay);
			CollectionAssert.AreEquivalent(list, plain);
		}
		
		#endregion
		
		#region Convertation
		
		[Test]
		[TestCase(TreeIteratingWay.Prefix, new int[] { 1, 2, 3, 4, 5, 6, 7 } )]
		[TestCase(TreeIteratingWay.Infix, new int[] { 3, 2, 4, 1, 6, 5, 7 } )]
		[TestCase(TreeIteratingWay.Postfix, new int[] { 3, 4, 2, 6, 7, 5, 1 } )]
		public void TreeTests_ToList(TreeIteratingWay iteratingWay, int[] list)
		{
			var tree = Prepare(1, 2, 3, 4, 5, 6, 7);
			var plain = tree.ToList(iteratingWay);
			CollectionAssert.AreEquivalent(list, plain);
		}
		
		#endregion
	}
}
