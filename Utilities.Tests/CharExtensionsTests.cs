﻿using System;
using NUnit.Framework;

using Utilities.Extensions;

namespace Utilities.Tests
{
	[TestFixture]
	public class CharExtensionsTests
	{
		[Test]
		[TestCase('A', 'a', 'z', true, false)]
		[TestCase('a', 'a', 'z', true, true)]
		[TestCase('b', 'a', 'z', false, true)]
		[TestCase('a', 'a', 'z', false, false)]
		public void BetweenTest(char input, char l, char r, bool isInc, bool result)
		{
			bool output = input.Between(l, r, isInc);
			Assert.AreEqual(result, output);
		}
	}
}
