﻿using System;
using System.Linq;
using NUnit.Framework;

using Utilities.Extensions;

namespace Utilities.Tests
{
	[TestFixture]
	public class ArrayExtensionsTests
	{
		#region Fields

		private static readonly int[] ARRAY_ONE = new int[] { 0, 1, 2, 3, 4};
		private static readonly int[,] ARRAY_TWO = new int[,] 
		{
			{ 0, 1, 2, 3, 4},
			{ 5, 6, 7, 8, 9}
		};

		#endregion
		
		#region Copy
		
		[Test]
		public void CopyTests()
		{
			var one = ARRAY_ONE.Copy();
			CollectionAssert.AreEquivalent(ARRAY_ONE, one);
			
			var two = ARRAY_ONE.Copy();
			CollectionAssert.AreEquivalent(ARRAY_ONE, one);
		}
		
		#endregion
	
		#region Clear

		[Test]
		[TestCase(0)]
		[TestCase(1)]
		public void ClearTests_OneDimension(int value)
		{
			var arr = ARRAY_ONE.Copy();
			arr.Clear(value);
			var exp = Enumerable.Repeat(value, ARRAY_ONE.Length).ToArray();
			CollectionAssert.AreEquivalent(exp, arr);
		}
		
		[Test]
		[TestCase(0)]
		[TestCase(1)]
		public void ClearTests_TwoDimensions(int value)
		{
			var arr = ARRAY_TWO.Copy();
			arr.Clear(value);
			
			int[,] exp = new int[ARRAY_TWO.GetLength(0), ARRAY_TWO.GetLength(1)];
			for (int i = 0; i < ARRAY_TWO.GetLength(0); i++)
				for (int j = 0; j < ARRAY_TWO.GetLength(1); j++)
					exp[i, j] = value;
			
			CollectionAssert.AreEquivalent(exp, arr);
		}
		
		#endregion
	
		#region Getting subarrays
		
		[Test]
		[TestCase(6)]
		[TestCase(-1)]
		public void GetRowTests_Exception(int index)
		{
			Assert.Throws(typeof(ArgumentOutOfRangeException),
			              () => ARRAY_TWO.GetRow(index));
		}
		
		[Test]
		[TestCase(0, new int[] { 0, 1, 2, 3, 4 })]
		[TestCase(1, new int[] { 5, 6, 7, 8, 9 })]
		public void GetRowTests_Success(int rowIndex, int[] result)
		{
			var row = ARRAY_TWO.GetRow(rowIndex);
			CollectionAssert.AreEquivalent(result, row);
		}
		
		[Test]
		[TestCase(6)]
		[TestCase(-1)]
		public void GetColumnTests_Exception(int index)
		{
			Assert.Throws(typeof(ArgumentOutOfRangeException),
			              () => ARRAY_TWO.GetColumn(index));
		}
		
		[Test]
		[TestCase(0, new int[] { 0, 5 })]
		[TestCase(1, new int[] { 1, 6 })]
		public void GetColumnTests_Success(int columnIndex, int[] result)
		{
			var col = ARRAY_TWO.GetColumn(columnIndex);
			CollectionAssert.AreEquivalent(result, col);
		}
		
		[Test]
		[TestCase(typeof(IndexOutOfRangeException), -1, 3)]
		[TestCase(typeof(IndexOutOfRangeException), 6, 3)]
		[TestCase(typeof(ArgumentOutOfRangeException), 2, 10)]
		public void GetRangeTests_Exception(Type exceptionType, int startIndex, int len)
		{
			Assert.Throws(exceptionType, 
			              () => ARRAY_ONE.GetRange(startIndex, len));
		}
		
		[Test]
		[TestCase(0, 5, new int[] { 0, 1, 2, 3, 4 })]
		[TestCase(0, 0, new int[] {})]
		[TestCase(1, 3, new int[] { 1, 2, 3 })]
		public void GetRangeTests_Success(int startIndex, int len, int[] result)
		{
			var range = ARRAY_ONE.GetRange(startIndex, len);
			CollectionAssert.AreEquivalent(result, range);
		}
		
		#endregion
		
		#region Convertion
		
		[Test]
		public void ToOneDimensionArray_Tests()
		{
			var res = Enumerable.Range(0, 10).ToArray();
			var arr = ARRAY_TWO.ToOneDimensionArray();
			CollectionAssert.AreEquivalent(res, arr);
		}
		
		#endregion
	
		#region Adding
		
		[Test]
		[TestCase(typeof(ArgumentException), new int[] { 1, 2, 3 })]
		[TestCase(typeof(ArgumentNullException), null)]
		public void AddRowTests_Exception(Type exceptionType, int[] row)
		{
			var newArr = ARRAY_TWO.Copy();
			Assert.Throws(exceptionType, () => newArr.AddRow(row));
		}
		
		[Test]
		public void AddRowTests_Success()
		{
			var newArr = ARRAY_TWO.Copy();
			int[] row = new int[] { 10, 11, 12, 13, 14 };
			newArr = newArr.AddRow(row);
			Assert.AreEqual(ARRAY_TWO.Length + row.Length, newArr.Length);
			Assert.AreEqual(ARRAY_TWO.GetLength(0) + 1, newArr.GetLength(0));
			Assert.AreEqual(row[2], newArr[ARRAY_TWO.GetLength(0), 2]);
		}
		
			[Test]
		[TestCase(typeof(ArgumentException), new int[] { 1, 2, 3 })]
		[TestCase(typeof(ArgumentNullException), null)]
		public void AddColumnTests_Exception(Type exceptionType, int[] row)
		{
			var newArr = ARRAY_TWO.Copy();
			Assert.Throws(exceptionType, () => newArr.AddColumn(row));
		}
		
		[Test]
		public void AddColumnTests_Success()
		{
			var newArr = ARRAY_TWO.Copy();
			int[] column = new int[] { 10, 11 };
			newArr = newArr.AddColumn(column);
			Assert.AreEqual(ARRAY_TWO.Length + column.Length, newArr.Length);
			Assert.AreEqual(ARRAY_TWO.GetLength(1) + 1, newArr.GetLength(1));
			Assert.AreEqual(column[1], newArr[1, ARRAY_TWO.GetLength(1)]);
		}
		
		#endregion
	}
}
