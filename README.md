# Common utilities
## Summary
_Version_: 0.4.8

_Description_: This is a set of extension methods that are required most often. Full list of utilities see below.

## Content
#### Extension methods:
All extensions methods are situated in the namespace `Utilities.Extensions`

> String
- `string Repeat(this string s, int count)`: Repeats input string for specific times
- `string NormalizeSpaces(this string s)`: Normalizes string by deleting double spaces
- `string Remove(this string s, string c)`: Removes all occurences of the specfic substring from string
- `string Capitalize(this string s, bool isJoin)`: Converts first letters of each word and removes spaces (optional)
- `string Decapitalize(this string s)`: Adds spaces and converts first letters of each word

> Char
- `bool Between(this char c, char left, char right, bool isIncluding = true)`: Checks if the specific character is in range

> IEnumerable
- `void ForEach<T>(this IEnumerable<T> sequence, Action<T> action)` and `void ForEach<T>(this IEnumerable<T> sequence, Action<int, T> action)`: Executes action for each item in the collection

> IList
- `void Toggle<TSource>(this IList<TSource> list, TSource item)`: Adds item to the collection if it is not yet there; removes item from the collection otherwise.
- `void RemoveAll<TSource>(this IList<TSource> list, TSource item)`: Removes from list all occurences of the specific item
- `void Remove<TSource>(this IList<TSource> list, IEnumerable<TSource> deleteList, bool isFirstOnly = false)`: Removes from the list all items form another enumeration

> IDictionary
- `string ToString<TKey, TValue>(this IDictionary<TKey, TValue> dictionary, string keyValuePairSeparator, string linesSeparator)`: Returns pretty string for the dictionary

> Array
- `T[] Copy<T>(this T[] array)` and `T[,] Copy<T>(this T[,] array)`: Returns new array with the same elements
- `void Clear<T>(this T[] array, T value)` and `void Clear<T>(this T[,] array, T value`: Resets array with specific value
- `T[] GetRow<T>(this T[,] array, int rowIndex)`: Gets row of two-dimensional array with the specific index
- `T[] GetColumn<T>(this T[,] array, int columnIndex)`: Gets column of two-dimensional array with the specific index
- `T[] GetRange<T>(this T[] array, int startIndex, int length)`: Returns subarray of one-dmensional array
- `T[] ToOneDimensionArray<T>(this T[,] array)`: Converts two-dimensional array to the one-dimentional array
- `T[,] AddRow<T>(this T[,] array)` and `T[,] AddRow<T>(this T[,] array, T[] row)`: Adds row to the array and returns new array
- `T[,] AddColumn<T>(this T[,] array)` and `T[,] AddColumn<T>(this T[,] array, T[] column)`: Adds column to the array and returns new array

#### Log helper
The utility is situated in the namespace `Utilities.Log`

> Properties:
- `Stream`: stream log message to be written to.
- `IsWithDate`: If `true` then message would contain Date and time of the logging

> Methods:
- `Log`: logs message to the stream. May contain arguments: message (requires); level in tree view (optional); message type (optional; either None|Info|Warning|Error)
- `Error`: simplified version of `Log` method for errors logging. Takes error message as a parameter
- `Empty`: prints empty lines. Takes number of lines as a parameter

> Using

1. Set stream
```
LogHelper.Stream = new FileStream("../../logger.log", FileMode.OpenOrCreate, FileAccess.ReadWrite);
```

2. Log neccessary data
```
LogHelper.Log("Test 1");
LogHelper.Empty(2);
LogHelper.Error(this.GetType(), "Error log message", 1);
```

3. Close log
```
LogHelper.Close();
```

#### Text file comparator
The utility is situated in the namespace `Utilities.TextFileComparator`

Use `TextFileComparator.CompareTextFiles` method to compare text files. 

* The method is static.
* Paths to the files are passed as parameters of the method.
* Return value has type `Mismatch` which is a struct with pointers to `Line` and `Symbol` in file where the mismatching is and `Error` type of it.

#### Tree helper
The utility is situated in the namespace `Utilities.TreeHelper`

This utility simplifies work with tree as recursive data structure

1. To create tree, use expression 
```
var tree = new BinaryTree<T>(rootNode);
```
where `rootNode` has type `BinaryTreeNode`. It has constructors with 3 arguments: entity itself, left branch, right branch.
or expression
```
var tree = new BinaryTree<int>(list);
```
where `list` is enumeration of tree nodes. This list is equivalent to the tree iterated in infix way.

2. To check if tree node is list, use read-only property `IsList`

3. To get or set default way of iterating, use `IteratingWay` property.

4. To iterate through the tree, use method `Run` with parameters:
* `Action<T> action`: executed action
* `TreeIteratingWay routing`: way of iterating (either prefix, infix or postfix; infix is default)

5. To convert tree to the plain list, use method `ToList`. The only paraneter is way of iterating through tree nodes (either prefix, infix or postfix; infix is default)